<?php

namespace app\widgets;

use yii\base\InvalidConfigException;
use yii\base\Widget;

/**
 * Class BooleanValue
 * @package app\widgets
 */
class BooleanValue extends Widget
{
    /**
     * @var int
     */
    public $value;

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        return $this->value == 1 ? '<i class="fa fa-check text-success fa-lg"></i>' : '<i class="fa fa-times text-danger fa-lg"></i>';
    }
}