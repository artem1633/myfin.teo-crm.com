<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm".
 *
 * @property int $id
 * @property int $contractor_id Клиент
 * @property int $company_id
 * @property int $services_id Сервис
 * @property string $project_desc Суть проетка
 * @property double $pay_all Оплата всего
 * @property double $payed Оплата оплачено
 * @property double $pay_remainder Оплата остаток
 * @property double $employee_pay_all Сотрудник расчет всего
 * @property double $employee_payed Сотрудник уже оплачено
 * @property double $employee_remainder Сотрудник Долг
 * @property string $work_start Дата начала работ
 * @property string $date_plan Срок сдачи план
 * @property string $date_fact Срок сдачи факт
 * @property string $status Статус
 * @property string $status_date Датастатуса
 * @property string $comment Примечания
 * @property int $user_id Сотрудник/исполнитель
 * @property int $projects_id проект
 *
 * @property Company $company
 * @property Contractor $contractor
 * @property Services $services
 * @property User $user
 */
class Crm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_id', 'company_id','projects_id', 'services_id', 'user_id'], 'integer'],
            [['project_desc', 'comment'], 'string'],
            [['pay_all', 'payed', 'pay_remainder', 'employee_pay_all', 'employee_payed', 'employee_remainder'], 'number'],
            [['work_start', 'date_plan', 'date_fact', 'status_date'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['services_id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['services_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['date_plan', 'date_fact','work_start','status_date','projects_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contractor_id' => 'Клиент',
            'company_id' => 'Company ID',
            'services_id' => 'Сервис',
            'project_desc' => 'Суть проетка',
            'pay_all' => 'Оплата всего',
            'payed' => 'Оплата оплачено',
            'pay_remainder' => 'Оплата остаток',
            'employee_pay_all' => 'Сотрудник расчет всего',
            'employee_payed' => 'Сотрудник уже оплачено',
            'employee_remainder' => 'Сотрудник Долг',
            'work_start' => 'Дата начала работ',
            'date_plan' => 'Срок сдачи план',
            'date_fact' => 'Срок сдачи факт',
            'status' => 'Статус',
            'status_date' => 'Дата статуса',
            'comment' => 'Примечания',
            'user_id' => 'Сотрудник/исполнитель',
            'projects_id' => 'Проект'
        ];
    }
    public function beforeSave($insert)
    {

        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasOne(Services::className(), ['id' => 'services_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return CrmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CrmQuery(get_called_class());
    }
}
