<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subgroup".
 *
 * @property int $id
 * @property string $name Название
 *  @property string $group_string Название
 * @property int $group_id Группа
 * @property int $company_id
 *
 * @property Company $company
 * @property Group $group
 */
class Subgroup extends \yii\db\ActiveRecord
{
    public $group_string;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subgroup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'company_id'], 'integer'],
            [['name','group_string'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'group_id' => 'Группа',
            'company_id' => 'Company ID',
            'group_string' => 'Новая группа'
        ];
    }

    public function beforeSave($insert)
    {

        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @inheritdoc
     * @return SubgroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubgroupQuery(get_called_class());
    }
    public function addGroup()
    {
        if ($this->group_string) {
            $group = new Group(['name' => $this->group_string]);
            if (!$group->save()) {
                \Yii::error($group->errors, '_error');
                return false;
            }
            $this->group_id = $group->id;
        }
        return true;
    }
}
