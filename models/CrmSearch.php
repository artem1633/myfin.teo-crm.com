<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Crm;

/**
 * CrmSearch represents the model behind the search form about `app\models\Crm`.
 */
class CrmSearch extends Crm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contractor_id', 'company_id', 'services_id', 'user_id','projects_id'], 'integer'],
            [['project_desc', 'work_start', 'date_plan', 'date_fact', 'status', 'status_date', 'comment'], 'safe'],
            [['pay_all', 'payed', 'pay_remainder', 'employee_pay_all', 'employee_payed', 'employee_remainder'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Crm::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contractor_id' => $this->contractor_id,
            'company_id' => $this->company_id,
            'services_id' => $this->services_id,
            'pay_all' => $this->pay_all,
            'payed' => $this->payed,
            'pay_remainder' => $this->pay_remainder,
            'employee_pay_all' => $this->employee_pay_all,
            'employee_payed' => $this->employee_payed,
            'employee_remainder' => $this->employee_remainder,
            'work_start' => $this->work_start,
            'date_plan' => $this->date_plan,
            'date_fact' => $this->date_fact,
            'status_date' => $this->status_date,
            'user_id' => $this->user_id,
            'projects_id' => $this->projects_id
        ]);

        $query->andFilterWhere(['like', 'project_desc', $this->project_desc])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
