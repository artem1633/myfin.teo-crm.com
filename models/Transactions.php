<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property int $id
 * @property string $pay_date Дата оплаты
 * @property string $date_fact Дата (Период) начисления (отгрузки)
 * @property double $sum Сумма
 * @property int $type Тип счета
 * @property int $contractor_id Клиент
 * @property int $article_id Статья
 * @property int $project_id Проект
 * @property int $pay_type Тип оплаты
 * @property string $comment Примечания
 * @property int $invoice_id Касса
 * @property int $company_id
 *
 * @property Article $article
 * @property Company $company
 * @property Contractor $contractor
 * @property Invoice $invoice
 * @property Projects $project
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_date', 'date_fact'], 'safe'],
            [['sum'], 'number'],
            [['type', 'contractor_id', 'article_id', 'project_id', 'pay_type', 'invoice_id', 'company_id'], 'integer'],
            [['comment'], 'string'],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['pay_date','date_fact','sum','article_id','project_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pay_date' => 'Дата оплаты',
            'date_fact' => 'Дата (Период) начисления (отгрузки)',
            'sum' => 'Сумма',
            'type' => 'Тип счета',
            'contractor_id' => 'Клиент',
            'article_id' => 'Статья',
            'project_id' => 'Проект',
            'pay_type' => 'Тип оплаты',
            'comment' => 'Примечания',
            'invoice_id' => 'Касса',
            'company_id' => 'Company ID',
        ];
    }
    public function beforeSave($insert)
    {

        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @inheritdoc
     * @return TransactionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransactionsQuery(get_called_class());
    }
}
