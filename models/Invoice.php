<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property string $name Название
 * * @property string $customer_string Название
 * @property string $comment Примечания
 * @property int $company_id
 * @property int $customer_id Юридическое лицо
 *
 * @property Company $company
 * @property Customers $customer
 */
class Invoice extends \yii\db\ActiveRecord
{
    public $customer_string;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment'], 'string'],
            [['company_id', 'customer_id'], 'integer'],
            [['name','customer_string'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'comment' => 'Примечания',
            'company_id' => 'Company ID',
            'customer_id' => 'Юридическое лицо',
            'customer_string' => 'Добавить Юридическое лицо'
        ];
    }
    public function beforeSave($insert)
    {

        if($this->isNewRecord){
            $this->company_id = Yii::$app->user->identity->company_id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }
    public function getFullName()
    {
        $customer = Customers::find()->where(['id' => $this->customer_id])->one();
        return $this->name . ' ( ' . $customer->name . ' )';
    }

    /**
     * @inheritdoc
     * @return InvoiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InvoiceQuery(get_called_class());
    }
    public function addCustomer()
    {
        if ($this->customer_string) {
            $group = new Customers(['name' => $this->customer_string]);
            if (!$group->save()) {
                \Yii::error($group->errors, '_error');
                return false;
            }
            $this->customer_id = $group->id;
        }
        return true;
    }
    public function getList()
    {
        return ArrayHelper::map(self::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name');
    }
}
