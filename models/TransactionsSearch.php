<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transactions;

/**
 * TransactionsSearch represents the model behind the search form about `app\models\Transactions`.
 */
class TransactionsSearch extends Transactions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'contractor_id', 'article_id', 'project_id', 'pay_type', 'invoice_id', 'company_id'], 'integer'],
            [['pay_date', 'date_fact', 'comment'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transactions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pay_date' => $this->pay_date,
            'date_fact' => $this->date_fact,
            'sum' => $this->sum,
            'type' => $this->type,
            'contractor_id' => $this->contractor_id,
            'article_id' => $this->article_id,
            'project_id' => $this->project_id,
            'pay_type' => $this->pay_type,
            'invoice_id' => $this->invoice_id,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
