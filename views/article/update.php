<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
?>
<div class="article-update">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => $action
    ]) ?>

</div>


