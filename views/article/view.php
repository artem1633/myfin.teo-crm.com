<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
?>
<div class="article-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'comment:ntext',
            'group_id',
            'subgroup_id',
            'company_id',
        ],
    ]) ?>

</div>
