<?php

use app\models\Group;
use app\models\Subgroup;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */


CrudAsset::register($this);
$session = Yii::$app->session;
?>
<?php Pjax::begin(['id' => 'article-pjax-container','enablePushState' => false]) ?>
<div class="article-form">

    <?php $form = ActiveForm::begin(['id' => 'frm', 'action' => ["{$action}"]]); ?>

    <div class="row"style="display: flex; align-items: center;">

        <div class="col-md-10">
            <?= $form->field($model, 'group_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Group::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                'language' => 'ru',
                'options' => ['placeholder' => 'выберите тип ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>

        <div class="col-md-2">
            <?= Html::a("<i class='fa fa-plus'></i>", ['article/create-group'], [
                'class' => 'btn btn-default btn-block',
                'style' => 'margin-top: 10px;',
                'role' => 'modal-remote',
                'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "article/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
            ]) ?>

        </div>

    </div>
    <div class="row"style="display: flex; align-items: center;">

        <div class="col-md-10">

            <?= $form->field($model, 'subgroup_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Subgroup::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                'language' => 'ru',
                'options' => ['placeholder' => 'выберите тип ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
        </div>


        <div class="col-md-2">
            <?= Html::a("<i class='fa fa-plus'></i>", ['article/create-subgroup'], [
                'class' => 'btn btn-default btn-block',
                'style' => 'margin-top: 10px;',
                'role' => 'modal-remote',
                'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "article/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
            ]) ?>

        </div>

    </div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton('Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

