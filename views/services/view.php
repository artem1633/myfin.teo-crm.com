<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
?>
<div class="services-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'company_id',
        ],
    ]) ?>

</div>
