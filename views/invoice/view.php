<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
?>
<div class="invoice-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'comment:ntext',
            'company_id',
            'customer_id',
        ],
    ]) ?>

</div>
