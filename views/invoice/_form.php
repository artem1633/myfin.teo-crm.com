<?php

use app\models\Customers;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invoice-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

   <div class="row">
       <div class="col-md-10">
           <div id="customer-select">
               <?php try {
                   echo  $form->field($model, 'customer_id')->widget(Select2::class, [
                       'data' => (new Customers())->list,
                       'options' => [
                           'placeholder' => 'Выберите юр лицо'
                       ]
                   ]);
               }catch (Exception $e) {
                   Yii::error($e->getMessage(), '_error');
                   echo $e->getMessage();
               }
               ?>
           </div>
           <div id="customer-input" style="display: none">
               <?= $form->field($model, 'customer_string')->textInput(); ?>
           </div>
       </div>
       <div class="col-md-2">
           <div class="switch-button" style="height: 7.5rem; align-items: center; display: flex;">
               <?= Html::button('<i class="fa fa-pencil"></i>', [
                   'id' => 'switch-button',
                   'data-action' => 'manual-edit',
                   'class' => 'btn btn-primary btn-block',
                   'title' => 'Ввести марку вручную',
               ]) ?>
           </div>
       </div>
   </div>
    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
$script = <<<JS
var customer_select = $('#customer-select');
var customer_input = $('#customer-input');
var switch_btn = $('#switch-button');

$(document).on('click', '[data-action="manual-edit"]', function() {
      customer_select.hide();
      customer_select.find('select').attr('selectedIndex', 0);
      customer_input.show();
      switch_btn.attr('data-action', 'select');
      switch_btn.attr('title', 'Выбрать из списка');
      switch_btn.find('i').toggleClass('fa-pencil').toggleClass('fa-reorder');
      return true;
});
$(document).on('click', '[data-action="select"]', function() {
      customer_select.show();
      customer_input.hide();
      customer_input.find('input').val('');
      switch_btn.attr('data-action', 'manual-edit');
      switch_btn.attr('title', 'Ввести вручную');
      switch_btn.find('i').toggleClass('fa-reorder').toggleClass('fa-pencil');
      return true;
});
JS;

$this->registerJs($script);