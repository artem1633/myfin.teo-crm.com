<?php

use app\models\Company;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
//     [
//     'class'=>'\kartik\grid\DataColumn',
//     'attribute'=>'id',
//     ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'content' => function ($model) {
            if ($model->is_super_company) {
                return '<i class="fa fa-star text-warning fa-lg"></i> ' . Html::a($model->name,
                        ['view', 'id' => $model->id], ['data-pjax' => 0]);
            }

            return Html::a($model->name, ['view', 'id' => $model->id], ['data-pjax' => 0]);
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address',
    ],
    [
        'label' => 'Пользователи',
        'content' => function ($model) {
            return Html::a('Показать', ['#'],
                    ['onclick' => 'event.preventDefault(); $(this).hide(); $(this).parent().find(".col-users-list").slideDown();'])
                . \app\widgets\CompanyUsers::widget([
                    'company' => $model,
                    'containerOptions' => ['class' => 'col-users-list', 'style' => 'display: none;']
                ]);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'api_key',
        'label' => 'АПИ',
        'content' => function (Company $model) {
            return Html::a($model->api_key ? 'Отключить доступ ' : 'Включить доступ к АПИ',
                ['/company/switch-api-access', 'id' => $model->id], [
                    'class' => '' . $model->api_key ? 'btn btn-small btn-block btn-warning' : 'btn btn-small btn-block btn-success',
                    'data-pjax' => 1,
                    'role' => 'modal-remote'
                ]);
         }
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'subscription_id',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_super_company',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'subscription_end_datetime',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'access',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'last_activity_datetime',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'balance',
    // ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Удалить',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Изменить',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                    ]) . "&nbsp;";
            }
        ],
    ],

];   