<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
?>
<div class="group-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'company_id',
        ],
    ]) ?>

</div>
