<?php

use app\models\Article;
use app\models\Transactions;
use Carbon\Carbon;
use yii\helpers\Html;

use leandrogehlen\treegrid\TreeGrid;

/* @var $this yii\web\View */
$this->title = 'Отчет ДДС';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-inverse report-project-index">
    <div class="panel-heading">
        <h4 class="panel-title">Отчет ДДС</h4>
    </div>
    <div class="panel-body">
        <div class="box box-default">
            <div class="box-body" style="overflow-x: auto;">

                <table id="table" class="table table-striped table-bordered">

                    <thead>
                    <tr>
                        <th>
                            Имя
                        </th>

                        <?php
                        foreach ($months as $month) {
                            ?>
                            <th>
                                <?= $month ?>
                            </th>
                            <?php
                        }
                        ?>

                    </tr>

                    </thead>
                    <tbody>

                    <?php
                    foreach ($groups as $group) {
                        ?>

                        <tr id="<?=$group->id?>" data-key="<?=$group->id?>">
                            <td>
                                <span style="cursor: pointer;" onclick="actFunction(event);"><i class="fa fa-plus-square"></i></span>

                                <?= $group->name ?>
                            </td>


                            <?php

                            foreach ($months as $month) {
                                $carbonMonth = Carbon::createFromFormat('Y-m-d', $month);

                                $groupSumm = 0;

                                foreach ($group->subgroups as $subgroup) {
                                    $articles = Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                    foreach ($articles as $article) {
                                        foreach ($dds as $d) {
                                            $carbonPayingdate = Carbon::createFromFormat('Y-m-d', $d->pay_date);
                                            if ($d->article_id == $article->id && $carbonMonth->month == $carbonPayingdate->month) {
                                                $groupSumm += $d->sum;
                                            }
                                            ?>

                                            <?php
                                        }
                                    }
                                }

                                echo('<td>' . $groupSumm . ' </td>');
                            }
                            ?>

                        </tr>
                        <?php
                        foreach ($group->subgroups as $subgroup) {
                            ?>

                            <tr class="parent-<?=$group->id?>"style="display:none;">
                                <td>
                                    <?= $subgroup->name ?>
                                </td>



                                <?php
                                $articles = Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                foreach ($months as $month) {
                                    $carbonMonth = Carbon::createFromFormat('Y-m-d', $month);

                                    $supgroupSumm = 0;

                                    foreach ($articles as $article) {
                                        foreach ($dds as $d) {
                                            $carbonPayingdate = Carbon::createFromFormat('Y-m-d', $d->pay_date);
                                            if ($d->article_id == $article->id && $carbonMonth->month == $carbonPayingdate->month) {
                                                $supgroupSumm += $d->sum;
                                            }
                                            ?>

                                            <?php
                                        }
                                    }

                                    echo('<td>' . $supgroupSumm . ' </td>');
                                }
                                ?>

                                <!--                                         ДЛЯ СЛЕДУЮЩИХ ЯЧЕЕК, В СТОЛБЦАХ ОБОЗНАЧАЮЩИХ МЕСЯЦ - ПОДГРУППЫ   -->

                            </tr>

                            <?php

                            $articles = Article::find()->where(['subgroup_id' => $subgroup->id])->all();
                            foreach ($articles as $article) {
                                ?>
                                <tr class="parent-<?=$subgroup->id?>"style="display: none">
                                    <td>
                                        <?= $article->name ?>
                                    </td>

                                    <?php
                                    foreach ($months as $month) {
                                        $carbonMonth = Carbon::createFromFormat('Y-m-d', $month);

                                        $articleSumm = 0;
                                        foreach ($dds as $d) {
                                            $carbonPayingdate = Carbon::createFromFormat('Y-m-d', $d->pay_date);
                                            if ($d->article_id == $article->id && $carbonMonth->month == $carbonPayingdate->month) {
                                                $articleSumm = $d->sum;
                                            }
                                            ?>

                                            <?php
                                        }

                                        echo('<td>' . $articleSumm . ' </td>');
                                    }
                                    ?>

                                </tr>

                                <?php
                            }
                            ?>

                            <?php
                        }
                        ?>

                        <?php
                    }
                    ?>


                    </tbody>

                </table>
            </div>
            <script>
                function actFunction(event) {
                    var clickedClass = event.target.className;
                    if (clickedClass == 'fa fa-minus-square'){
                        $(event.currentTarget).html("<i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>");

                        var parId = $(event.currentTarget).closest('tr').attr('data-key');

                        var childs = $(`.parent-${parId}`);
                        childs.hide();
                    }else {
                        $(event.currentTarget).html("<i class=\"fa fa-minus-square\"></i>");

                        var parId = $(event.currentTarget).closest('tr').attr('data-key');

                        var childs = $(`.parent-${parId}`);
                        childs.show();
                    }
                }
            </script>
        </div>

</div>


