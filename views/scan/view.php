<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Scan */
?>
<div class="scan-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'loaded_at',
            'link',
            'author_id',
            'bid_id',
        ],
    ]) ?>

</div>
