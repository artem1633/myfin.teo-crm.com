<?php

use app\admintheme\grid\GridView;

use app\models\Company;

use kartik\select2\Select2;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Табель';

\app\assets\plugins\TipperAsset::register($this);

?>
<div class="table-tabs"style="margin-top: -45px">


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Табель</h4>
                    <div class="panel-heading-btn" style="margin-top:-20px;overflow-y: auto;">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                </div>
                <div class="panel-body" >
                    <div class="col-md-12">
                        <?php $this->beginContent('@app/views/table/status.php',[
                            //                            'model' => $model,
                            'dataProvider' => $dataProvider,
                        ]); ?>
                        <?php $this->endContent(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>



<?php

$script = <<< JS
// data-popup-role

      tippy('[data-popup-role]', {
        content: 'Загрузка ...',
        allowHTML: true,
        onShow: function(){
            
            
            
            
        }
      });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>