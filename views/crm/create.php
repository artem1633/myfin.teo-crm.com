<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Crm */

?>
<div class="crm-create">
    <?= $this->render('_form', [
        'model' => $model,
        'action' => $action
    ]) ?>
</div>
