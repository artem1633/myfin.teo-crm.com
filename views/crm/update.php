<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Crm */
?>
<div class="crm-update">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => $action
    ]) ?>

</div>
