<?php

use app\models\Contractor;
use app\models\Projects;
use app\models\Services;
use app\models\User;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Crm */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */

CrudAsset::register($this);
?>
<?php Pjax::begin(['id' => 'crm-pjax-container','enablePushState' => false]) ?>
<div class="crm-form">

    <?php $form = ActiveForm::begin(['id' => 'frm', 'action' => ["crm/{$action}"]]); ?>
    <div class="row">
        <div class="col-md-3">
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-md-10 "style="padding:0 6px ;">
                    <?= $form->field($model, 'contractor_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Contractor::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                        'language' => 'ru',
                        'options' => ['placeholder' => 'выберите контрагента ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
                <div class="col-md-2" style="padding: 0">
                    <?= Html::a("<i class='fa fa-plus'></i>", ['crm/create-contractor'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 10px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "crm/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>

                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-md-10 "style="padding:0 6px ;">
                    <?= $form->field($model, 'projects_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Projects::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                        'language' => 'ru',
                        'options' => ['placeholder' => 'выберите проект ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
                <div class="col-md-2" style="padding: 0">
                    <?= Html::a("<i class='fa fa-plus'></i>", ['crm/create-projects'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 10px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "crm/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-md-10 "style="padding:0 6px ;">

                    <?= $form->field($model, 'services_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Services::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                        'language' => 'ru',
                        'options' => ['placeholder' => 'выберите услугу ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
                <div class="col-md-2" style="padding: 0">
                    <?= Html::a("<i class='fa fa-plus'></i>", ['crm/create-services'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 10px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "crm/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')->dropDownList([
                'в работе' => 'в работе',
                'пауза' => 'пауза',
                'завершен' => 'завершен'
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status_date')->input('date') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'project_desc')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'pay_all')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'payed')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'pay_remainder')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'employee_pay_all')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'employee_payed')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'employee_remainder')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'work_start')->input('date') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'date_plan')->input('date') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'date_fact')->input('date') ?>
        </div>
        <div class="col-md-3">
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-md-10 "style="padding:0 6px ;">
                    <?php
                    try {
                        echo $form->field($model, 'user_id')->widget(Select2::class, [
                            'data' => (new User())->getList(),
                            'options' => [
                                'id' => 'user',
                                'prompt' => 'Выберите пользователя'
                            ]
                        ])->label('Имя пользователя');
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    } ?>


                </div>
                <div class="col-md-2" style="padding: 0">
                    <?= Html::a("<i class='fa fa-plus'></i>", ['crm/create-user'], [
                        'class' => 'btn btn-default btn-block',
                        'style' => 'margin-top: 10px;',
                        'role' => 'modal-remote',
                        'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "crm/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                    ]) ?>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php Pjax::end(); ?>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>


