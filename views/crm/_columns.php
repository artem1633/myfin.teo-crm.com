<?php

use app\models\Contractor;
use app\models\Services;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'contractor_id',
        'content' => function($data){

            $company = ArrayHelper::getColumn(Contractor::find()->where(['id' => $data->contractor_id])->all(), 'name');
            return implode('',$company);


        },
        'filter' => ArrayHelper::map(Contractor::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'services_id',
        'content' => function($data){

            $company = ArrayHelper::getColumn(Services::find()->where(['id' => $data->services_id])->all(), 'name');
            return implode('',$company);


        },
        'filter' => ArrayHelper::map(Services::find()->asArray()->all(), 'id', 'name'),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => ''],
            'pluginOptions' => ['allowClear' => true],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'project_desc',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pay_all',
    ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'payed',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'pay_remainder',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'employee_pay_all',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'employee_payed',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'employee_remainder',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'work_start',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'date_plan',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'date_fact',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'status',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'status_date',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'comment',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'user_id',
//     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
            }
        ],
    ],

];   