<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Crm */
?>
<div class="crm-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'contractor_id',
            'company_id',
            'services_id',
            'project_desc:ntext',
            'pay_all',
            'payed',
            'pay_remainder',
            'employee_pay_all',
            'employee_payed',
            'employee_remainder',
            'work_start',
            'date_plan',
            'date_fact',
            'status',
            'status_date',
            'comment:ntext',
            'user_id',
        ],
    ]) ?>

</div>
