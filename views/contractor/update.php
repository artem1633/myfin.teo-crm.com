<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contractor */
?>
<div class="contractor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
