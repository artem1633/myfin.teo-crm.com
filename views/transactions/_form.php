<?php

use app\models\Article;
use app\models\Contractor;
use app\models\Invoice;
use app\models\Projects;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Transactions */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */

CrudAsset::register($this);
?>
<?php Pjax::begin(['id' => 'transactions-pjax-container','enablePushState' => false]) ?>
    <div class="transactions-form">

        <?php $form = ActiveForm::begin(['id' => 'frm', 'action' => ["transactions/{$action}"]]); ?>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'pay_date')->input('date') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'date_fact')->input('date')  ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'sum')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'type')->dropDownList([
                    0 => 'с НДС',
                    1 => 'без НДС'
                ])?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'pay_type')->dropDownList([
                    0 => 'Наличный расчет',
                    1 =>  'Безналичный расчет'
                ])?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="row" style="display: flex; align-items: center;">
                    <div class="col-md-10 "style="padding:0 6px ;">
                        <?= $form->field($model, 'contractor_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Contractor::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                            'language' => 'ru',
                            'options' => ['placeholder' => 'выберите контрагента ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);?>
                    </div>
                    <div class="col-md-2" style="padding: 0">
                        <?= Html::a("<i class='fa fa-plus'></i>", ['transactions/create-contractor'], [
                            'class' => 'btn btn-default btn-block',
                            'style' => 'margin-top: 10px;',
                            'role' => 'modal-remote',
                            'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "transactions/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                        ]) ?>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row" style="display: flex; align-items: center;">
                    <div class="col-md-10 "style="padding:0 6px ;">

                        <?= $form->field($model, 'article_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Article::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                            'language' => 'ru',
                            'options' => ['placeholder' => 'выберите статью ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);?>
                    </div>
                    <div class="col-md-2" style="padding: 0">
                        <?= Html::a("<i class='fa fa-plus'></i>", ['transactions/create-article'], [
                            'class' => 'btn btn-default btn-block',
                            'style' => 'margin-top: 10px;',
                            'role' => 'modal-remote',
                            'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "transactions/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                        ]) ?>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row" style="display: flex; align-items: center;">
                    <div class="col-md-10 "style="padding:0 6px ;">
                        <?= $form->field($model, 'project_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Projects::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                            'language' => 'ru',
                            'options' => ['placeholder' => 'выберите проект ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);?>
                    </div>
                    <div class="col-md-2" style="padding: 0">
                        <?= Html::a("<i class='fa fa-plus'></i>", ['transactions/create-project'], [
                            'class' => 'btn btn-default btn-block',
                            'style' => 'margin-top: 10px;',
                            'role' => 'modal-remote',
                            'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "transactions/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                        ]) ?>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row" style="display: flex; align-items: center;">
                    <div class="col-md-10 "style="padding:0 6px ;">
                        <?= $form->field($model, 'invoice_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Invoice::find()->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id])->all(), 'id', 'name'),
                            'language' => 'ru',
                            'options' => ['placeholder' => 'выберите кассу ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);?>
                    </div>
                    <div class="col-md-2" style="padding: 0">
                        <?= Html::a("<i class='fa fa-plus'></i>", ['transactions/create-invoice'], [
                            'class' => 'btn btn-default btn-block',
                            'style' => 'margin-top: 10px;',
                            'role' => 'modal-remote',
                            'onclick' => '
                        
                                    var $form = $("#frm");
                                    $.ajax({
                                      type: "POST",
                                      url: "transactions/session-form",
                                      data: $form.serialize(),
                                    }).done(function() {
                                      console.log(\'success\');
                                    }).fail(function() {
                                      console.log(\'fail\');
                                    });
                        ',
                        ]) ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <?php if (!Yii::$app->request->isAjax){ ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
<?php Pjax::end(); ?>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

