<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transactions */
?>
<div class="transactions-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pay_date',
            'date_fact',
            'sum',
            'type',
            'contractor_id',
            'article_id',
            'project_id',
            'pay_type',
            'comment:ntext',
            'invoice_id',
            'company_id',
        ],
    ]) ?>

</div>
