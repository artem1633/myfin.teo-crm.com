<?php

use app\models\Article;
use app\models\Transactions;
use yii\helpers\Html;

use leandrogehlen\treegrid\TreeGrid;

/* @var $this yii\web\View */
$this->title = 'Отчет проектов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-inverse report-project-index">
    <div class="panel-heading">
        <h4 class="panel-title">Отчет проектов</h4>
    </div>
    <div class="panel-body">
        <div class="box box-default">
            <div class="box-body" style="overflow-x: auto;">

                <table id="table" class="table table-striped table-bordered">

                    <thead>
                    <tr>
                        <th>
                            Имя
                        </th>

                        <th>
                            Итого
                        </th>

                        <?php
                        foreach ($projects as $project) {
                            ?>
                            <th>
                                <?= $project->name ?>
                            </th>
                            <?php
                        }
                        ?>

                    </tr>

                    </thead>
                    <tbody>

                    <?php
                    foreach ($groups as $group) {
                        ?>

                        <tr id="<?=$group->id?>" data-key="<?=$group->id?>">
                            <td>
                                <span style="cursor: pointer;" onclick="actFunction(event);"><i class="fa fa-plus-square"></i></span>
                                <?= $group->name ?>
                            </td>

                            <td>

                            </td>

                            <?php

                            foreach ($projects as $project) {

                                $groupSumm = 0;

                                $articles = Article::find()->where(['subgroup_id' => $group->id])->all();

                                foreach ($group->subgroups as $subgroup) {
                                    $articles = Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                    foreach ($articles as $article) {
                                        foreach ($dds as $d) {

                                            if ($d->article_id == $article->id && $project->id == $d->project_id) {
                                                $groupSumm = $groupSumm + $d->sum;
                                            }
                                            ?>

                                            <?php
                                        }
                                    }
                                }

                                echo('<td>' . $groupSumm . ' </td>');
                            }
                            ?>

                        </tr>
                        <?php
                        foreach ($group->subgroups as $subgroup) {
                            ?>

                            <tr class="parent-<?=$group->id?>"style="display:none;">
                                <td>
                                    <?= $subgroup->name ?>
                                </td>

                                <td>

                                </td>

                                <?php

                                $articles = Article::find()->where(['subgroup_id' => $subgroup->id])->all();

                                foreach ($projects as $project) {

                                    $supgroupSumm = 0;

                                    foreach ($articles as $article) {
                                        foreach ($dds as $d) {
                                            if ($d->article_id == $article->id && $project->id == $d->project_id) {
                                                $supgroupSumm += $d->sum;
                                            }
                                            ?>

                                            <?php
                                        }
                                    }

                                    echo('<td>' . $supgroupSumm . ' </td>');
                                }
                                ?>

                                <!--                                         ДЛЯ СЛЕДУЮЩИХ ЯЧЕЕК, В СТОЛБЦАХ ОБОЗНАЧАЮЩИХ МЕСЯЦ - ПОДГРУППЫ   -->

                            </tr>

                            <?php

                            $articles = Article::find()->where(['subgroup_id' => $subgroup->id])->all();
                            foreach ($articles as $article) {
                                ?>
                                <tr class="parent-<?=$subgroup->id?>"style="display:none;">
                                    <td>
                                        <span class="treegrid-indent">

                                        </span>
                                        <span class="treegrid-indent">

                                        </span>
                                        <span class="treegrid-expander">

                                        </span>

                                        <?= $article->name ?>
                                    </td>


                                    <?php
                                    $summByArticle = 0;
                                    foreach ($projects as $project) {
                                        $ddsByArticleAndProject = Transactions::find()->where(['project_id' => $project->id, 'article_id' => $article->id])->all();

                                        foreach ($ddsByArticleAndProject as $d) {
                                            $summByArticle += $d->sum;
                                        }

                                    }
                                    ?>


                                    <td>
                                        <?= $summByArticle ?>
                                    </td>

                                    <?php
                                    foreach ($projects as $project) {

                                        $articleSumm = 0;
                                        foreach ($dds as $d) {
                                            if ($d->article_id == $article->id && $project->id == $d->project_id) {
                                                $articleSumm = $d->sum;
                                            }
                                            ?>

                                            <?php
                                        }

                                        echo('<td>' . $articleSumm . ' </td>');
                                    }
                                    ?>

                                </tr>

                                <?php
                            }
                            ?>

                            <?php
                        }
                        ?>

                        <?php
                    }
                    ?>


                    </tbody>

                </table>
            </div>
        </div>


        <script>
            function actFunction(event) {
                var clickedClass = event.target.className;
                if (clickedClass == 'fa fa-minus-square'){
                    $(event.currentTarget).html("<i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>");

                    var parId = $(event.currentTarget).closest('tr').attr('data-key');

                    var childs = $(`.parent-${parId}`);
                    childs.hide();
                }else {
                    $(event.currentTarget).html("<i class=\"fa fa-minus-square\"></i>");

                    var parId = $(event.currentTarget).closest('tr').attr('data-key');

                    var childs = $(`.parent-${parId}`);
                    childs.show();
                }
            }
        </script>



    </div>
</div>


