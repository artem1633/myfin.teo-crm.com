<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
?>
<div class="projects-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'comment:ntext',
            'company_id',
        ],
    ]) ?>

</div>
