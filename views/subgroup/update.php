<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subgroup */
?>
<div class="subgroup-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
