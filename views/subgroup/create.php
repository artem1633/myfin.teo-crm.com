<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Subgroup */

?>
<div class="subgroup-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
