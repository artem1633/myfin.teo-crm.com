<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Subgroup */
?>
<div class="subgroup-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'group_id',
            'company_id',
        ],
    ]) ?>

</div>
