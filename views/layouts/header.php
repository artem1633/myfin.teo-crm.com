<?php

use app\models\Objects;
use app\models\Technic;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\Users;

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                MyFin
            </a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- end mobile sidebar expand / collapse button -->
        <?php if(Yii::$app->user->isGuest == false): ?>
            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right">
				<li class="date-now"><?php echo date('d.m.Y H:i'); ?></li>
                <li class="navbar-user"style="padding-top: 20px"><?=Yii::$app->user->identity->company->name?></li>


                <li class="navbar-user">
                    <?= Html::a('<i class="fa fa-bell" style="font-size: 20px;"></i>', '#', ['title' => 'Уведомления', 'style' => 'cursor: pointer; position: relative;', 'onclick' => "event.preventDefault(); $('.header-messages').toggle();"]) ?>
                </li>
                <li class="dropdown navbar-user">
                    <div class="header-messages" style="display: none;position: absolute;background: #fff;border-radius: 3px;box-shadow: 0 0 10px rgba(0,0,0,0.5);height: 260px;width: 260px;margin-top: 44px;right: 1px;overflow-y: scroll;">

                    </div>
                </li>



                <li class="dropdown navbar-user">
                    <a id="btn-user-dropdown" href="javascript:;">
                        <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view" alt="">
                        <span class="hidden-xs"><?=Yii::$app->user->identity->email?></span> <b class="caret"></b>
                    </a>
                    <ul id="dropdown-user-menu" class="dropdown-menu animated fadeInLeft" style="">
                        <li class="arrow"></li>
                        <li> <?= Html::a('Настройки пользователи', ['user/profile']) ?> </li>
                        <li> <?= Html::a('Тарифы', ['#']) ?> </li>
                        <li class="divider"></li>
                        <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                    </ul>

                </li>
            </ul>
            <!-- end header navigation right -->
        <?php endif; ?>
    </div>
    <!-- end container-fluid -->
</div>

<!-- Cleversite chat button -->
	<script type='text/javascript'>
		(function() { 
			var s = document['createElement']('script');
			s.type = 'text/javascript'; 
			s.async = true; 
			s.charset = 'utf-8';	
			s.src = '//cleversite.ru/cleversite/widget_new.php?supercode=1&referer_main='+encodeURIComponent(document.referrer)+'&clid=58510bRHNK&siteNew=76381'; 
			var ss = document['getElementsByTagName']('script')[0]; 
			if(ss)
 {
				ss.parentNode.insertBefore(s, ss);
			} else {
				document.documentElement.firstChild.appendChild(s);
			};
		})(); 
	</script>
<!-- / End of Cleversite chat button -->