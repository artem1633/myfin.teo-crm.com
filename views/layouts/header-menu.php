<?php

use app\admintheme\widgets\Menu;

?>

<div id="sidebar" class="sidebar">
    <?php if (Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo Menu::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav'],
                    'items' => [
                        ['label' => 'CRM', 'url' => ['/crm'],'icon' => 'fa fa-credit-card'],
                        ['label' => 'Реестр операций', 'url' => ['/transactions'],'icon' => 'fa fa-book'],
                        ['label' => 'Табель', 'url' => ['/table'], 'icon' => '  fa fa-table'],
                        [
                            'label' => 'Отчеты',
                            'icon' => 'fa fa-book',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'items' => [
                                ['label' => 'ДДС', 'url' => ['/report-dds']],
                                ['label' => 'Проекты', 'url' => ['/report-project']],

                            ],
                        ],
                        ['label' => 'Пользователи', 'url' => ['/user'], 'icon' => 'fa fa-users'],
                        [
                            'label' => 'Моя компания',
                            'url' => ['/company'],
                            'icon' => 'fa fa-building-o',
                            'visible' => Yii::$app->user->identity->isSuperAdmin() == false
                        ],


                        [
                            'label' => 'Компании',
                            'icon' => 'fa fa-building-o',
                            'url' => ['/company'],

                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],

                        ['label' => 'Обратная связь', 'url' => ['/ticket'], 'icon' => 'fa fa-question'],


                        [
                            'label' => 'Справочники',
                            'icon' => 'fa fa-book',
                            'url' => '#',
                            'options' => ['class' => 'has-sub'],
                            'items' => [
                                ['label' => 'Должности', 'url' => ['/position']],
                                ['label' => 'Счета', 'url' => ['/invoice']],
                                ['label' => 'Статьи', 'url' => ['/article']],
                                ['label' => 'Проекты', 'url' => ['/projects']],
                                ['label' => 'Юр. лица', 'url' => ['/customers']],
                                ['label' => 'Контрагенты', 'url' => ['/contractor']],
                                ['label' => 'Услуги', 'url' => ['/services']],
                                ['label' => 'Группы', 'url' => ['/group']],
                                ['label' => 'Подгуппы', 'url' => ['/subgroup']],

                                [
                                    'label' => 'Документация АПИ',
                                    'url' => ['/api/doc'],
                                    'visible' => Yii::$app->user->identity->company->api_key ?? false
                                ],
                            ],
                        ],

                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
