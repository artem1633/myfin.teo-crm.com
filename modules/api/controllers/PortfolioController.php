<?php

namespace app\modules\api\controllers;

use Yii;
use app\components\helpers\AnswerFinder;
use app\models\Bot;
use app\models\BotScenario;
use app\models\BotSetting;
use app\models\Chat;
use app\models\ChatHistory;
use app\models\Company;
use app\models\Customer;
use app\models\Portfolio;
use yii\rest\ActiveController;
use yii\web\Response;
use app\models\User;
use app\models\PortfolioRate;
use yii\helpers\VarDumper;

/**
 * Default controller for the `api` module
 */
class PortfolioController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionCheckStart()
    {
        /** @var Portfolio[] $portfolios */
        $portfolios = Portfolio::find()->where(['status' => Portfolio::STATUS_MODERATED])->andWhere(['<=', 'datetime_start', date('Y-m-d H:i:s')])->all();

        foreach ($portfolios as $portfolio){
            $portfolio->status = Portfolio::STATUS_RATING;
            $portfolio->detachBehavior('company_id');
            $portfolio->save(false);

            $portfolio->company->sendEmailMessage("Начало торгов по портфелю «{$portfolio->name}»", "
                <p>Доброго времени суток, {$portfolio->company->director}!</p>
                <p>Торги по вашему потрфелю №{$portfolio->id} «{$portfolio->name}» начались</p>
            ");
        }
    }

    public function actionSpotWinner()
    {
        /** @var Portfolio[] $portfolios */
        $portfolios = Portfolio::find()->where(['status' => Portfolio::STATUS_RATING])->andWhere(['<=', 'datetime_end', date('Y-m-d H:i:s')])->all();

        foreach ($portfolios as $portfolio) {
            $rate = PortfolioRate::find()->where(['portfolio_id' => $portfolio->id])->orderBy('amount desc')->one();

            if($rate == null){
                $portfolio->status = Portfolio::STATUS_DONE_NO_DEAL;
                $portfolio->stage = Portfolio::STAGE_START;
                $portfolio->company->sendEmailMessage("Торги по портфелю №{$portfolio->id} «{$portfolio->name}» завершены", "
                        <p>Доброго времени суток, {$portfolio->company->director}!</p>
                        <p>Торги по портфелю №{$portfolio->id} «{$portfolio->name}». Победитель не определен в связи с отсутствием ставок</p>
                    ");

                $portfolio->save(false);

                continue;
            }

            $winner = User::findOne($rate->user_id);

            if($winner != null){
                $portfolio->winner_id = $winner->company_id;

                $portfolio->status = Portfolio::STATUS_DONE;
                $portfolio->stage = Portfolio::STAGE_START;
                $portfolio->company->sendEmailMessage("Торги по портфелю №{$portfolio->id} «{$portfolio->name}» завершены", "
                        <p>Доброго времени суток, {$portfolio->company->director}!</p>
                        <p>Торги по портфелю №{$portfolio->id} «{$portfolio->name}». Победитель определен это — {$winner->fio} (Компания: {$winner->company->name})</p>
                    ");

                $portfolio->save(false);

                $winner->sendEmailMessage("Вы выиграли аукцион по портелю №{$portfolio->id} «{$portfolio->name}»", "
                        <p>Доброго времени суток, {$winner->fio}!</p>
                        <p>Поздравляем, вы выиграли портфель «{$portfolio->name}».</p>
                        <p>Зайдите в личный кабинет во вкладку «Мои сделки» для заключения договора</p>
                    ");
            }
        }
    }
}
