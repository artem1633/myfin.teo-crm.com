<?php


namespace app\controllers;


use app\models\Group;
use app\models\Projects;
use app\models\ProjectsSearch;
use app\models\Transactions;
use Yii;
use yii\web\Controller;
use Carbon\Carbon;

class ReportDdsController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id]);

        $projects = Projects::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
        $groups = Group::find()->joinWith('subgroups')->joinWith('articles')->where(['group.company_id' => Yii::$app->user->identity->company_id])->all();

        $dds = Transactions::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();

        $carbonDate = Carbon::now();
        $months = array();

        for ($i=0; $i<10; $i++) {
            $months[] = $carbonDate->format('Y-m-01');
            $carbonDate->addMonth();
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'groups' => $groups,
            'dds' => $dds,
            'projects' => $projects,
            'months' => $months,
        ]);
    }
}