<?php


namespace app\controllers;

use app\models\Group;
use app\models\Projects;
use app\models\ProjectsSearch;
use app\models\Transactions;
use Yii;
use yii\web\Controller;

class ReportProjectController extends Controller
{
    public function actionIndex()
    {
        $groups = Group::find()->joinWith('subgroups')->joinWith('articles')->where(['group.company_id' => Yii::$app->user->identity->company_id])->all();


        $dds = Transactions::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();
        $projects = Projects::find()->where(['company_id' => Yii::$app->user->identity->company_id])->all();

        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['company_id' => Yii::$app->user->identity->company_id]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'groups' => $groups,
            'dds' => $dds,
            'projects' => $projects,
        ]);
    }
}