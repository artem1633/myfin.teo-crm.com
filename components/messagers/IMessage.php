<?php

namespace app\components\messagers;

use yii\base\Component;

abstract class IMessage extends Component
{
    public $customer;

    public $botSetting;

    /**
     * @param integer $userId
     * @param string $text
     * @param array $options
     * @return mixed
     */
    public abstract function sendMessage($userId, $text, $options = null);

    /**
     * @param $photoPath
     * @return $mixed
     */
    public abstract function sendFile($photoPath);
}