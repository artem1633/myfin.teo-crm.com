<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class D3Asset
 * @package app\plugins\assets
 */
class D3Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'libs/d3/d3.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
