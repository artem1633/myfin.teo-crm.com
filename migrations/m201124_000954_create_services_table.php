<?php

use yii\db\Migration;

/**
 * Handles the creation of table `services`.
 */
class m201124_000954_create_services_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('services', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-services-company_id',
            'services',
            'company_id'
        );

        $this->addForeignKey(
            'fk-services-company_id',
            'services',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-services-company_id',
            'services'
        );

        $this->dropIndex(
            'idx-services-company_id',
            'services'
        );
        $this->dropTable('services');
    }
}
