<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article`.
 */
class m201124_011955_create_article_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'comment' => $this->text()->comment('Примечания'),
            'group_id' => $this->integer()->comment('Группа'),
            'subgroup_id' => $this->integer()->comment('Подгруппа'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-article-group_id',
            'article',
            'group_id'
        );

        $this->addForeignKey(
            'fk-article-group_id',
            'article',
            'group_id',
            'group',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-article-subgroup_id',
            'article',
            'subgroup_id'
        );

        $this->addForeignKey(
            'fk-article-subgroup_id',
            'article',
            'subgroup_id',
            'subgroup',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-article-company_id',
            'article',
            'company_id'
        );

        $this->addForeignKey(
            'fk-article-company_id',
            'article',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-article-company_id',
            'article'
        );

        $this->dropIndex(
            'idx-article-company_id',
            'article'
        );
        $this->dropForeignKey(
            'fk-article-group_id',
            'article'
        );

        $this->dropIndex(
            'idx-article-group_id',
            'article'
        );
        $this->dropForeignKey(
            'fk-article-subgroup_id',
            'article'
        );

        $this->dropIndex(
            'idx-article-subgroup_id',
            'article'
        );
        $this->dropTable('article');
    }
}
