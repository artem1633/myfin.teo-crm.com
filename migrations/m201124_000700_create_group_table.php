<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group`.
 */
class m201124_000700_create_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-group-company_id',
            'group',
            'company_id'
        );

        $this->addForeignKey(
            'fk-group-company_id',
            'group',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-group-company_id',
            'group'
        );

        $this->dropIndex(
            'idx-group-company_id',
            'group'
        );
        $this->dropTable('group');
    }
}
