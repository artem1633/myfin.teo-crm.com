<?php

use yii\db\Migration;

/**
 * Class m201201_005627_add_crm_column
 */
class m201201_005627_add_crm_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crm', 'projects_id', $this->integer()->comment('Проект'));
        $this->createIndex(
            'idx-crm-projects_id',
            'crm',
            'projects_id'
        );

        $this->addForeignKey(
            'fk-crm-projects_id',
            'crm',
            'projects_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-crm-projects_id',
            'crm'
        );

        $this->dropIndex(
            'idx-crm-projects_id',
            'crm'
        );
        $this->dropColumn('crm', 'projects_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201201_005627_add_crm_column cannot be reverted.\n";

        return false;
    }
    */
}
