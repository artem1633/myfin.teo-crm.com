<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transactions`.
 */
class m201124_025200_create_transactions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transactions', [
            'id' => $this->primaryKey(),
            'pay_date' => $this->date()->comment('Дата оплаты'),
            'date_fact' => $this->date()->comment('Дата (Период) начисления (отгрузки)'),
            'sum' => $this->float()->comment('Сумма'),
            'type' => $this->integer()->comment('Тип счета'),
            'contractor_id' => $this->integer()->comment('Клиент'),
            'article_id' => $this->integer()->comment('Статья'),
            'project_id' => $this->integer()->comment('Проект'),
            'pay_type' => $this->integer()->comment('Тип оплаты'),
            'comment' => $this->text()->comment('Примечания'),
            'invoice_id' => $this->integer()->comment('Касса'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-transactions-company_id',
            'transactions',
            'company_id'
        );

        $this->addForeignKey(
            'fk-transactions-company_id',
            'transactions',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-transactions-invoice_id',
            'transactions',
            'invoice_id'
        );

        $this->addForeignKey(
            'fk-transactions-invoice_id',
            'transactions',
            'invoice_id',
            'invoice',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-transactions-contractor_id',
            'transactions',
            'contractor_id'
        );

        $this->addForeignKey(
            'fk-transactions-contractor_id',
            'transactions',
            'contractor_id',
            'contractor',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-transactions-article_id',
            'transactions',
            'article_id'
        );

        $this->addForeignKey(
            'fk-transactions-article_id',
            'transactions',
            'article_id',
            'article',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-transactions-project_id',
            'transactions',
            'project_id'
        );

        $this->addForeignKey(
            'fk-transactions-project_id',
            'transactions',
            'project_id',
            'projects',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-transactions-company_id',
            'transactions'
        );

        $this->dropIndex(
            'idx-transactions-company_id',
            'transactions'
        );
        $this->dropForeignKey(
            'fk-transactions-invoice_id',
            'transactions'
        );

        $this->dropIndex(
            'idx-transactions-invoice_id',
            'transactions'
        );
        $this->dropForeignKey(
            'fk-transactions-contractor_id',
            'transactions'
        );

        $this->dropIndex(
            'idx-transactions-contractor_id',
            'transactions'
        );
        $this->dropForeignKey(
            'fk-transactions-article_id',
            'transactions'
        );

        $this->dropIndex(
            'idx-transactions-article_id',
            'transactions'
        );
        $this->dropTable('transactions');
    }
}
