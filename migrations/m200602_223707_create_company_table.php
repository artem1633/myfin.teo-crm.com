<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m200602_223707_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'address' => $this->string()->comment('Адрес'),
            'post_index' => $this->string()->comment('Почтовый индекс'),
            'is_super_company' => $this->boolean()->defaultValue(false)->comment('Является ли супер компанией'),
            'access' => $this->boolean()->defaultValue(true)->comment('Доступ (вкл/выкл)'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'balance' => $this->float()->comment('Баланс'),
            'created_at' => $this->dateTime(),
            'code' => $this->string()->comment('Код'),
            'inn'=> $this->string()->comment('ИНН'),
            'ogrn'=> $this->string()->comment('ОГРН'),
            'kpp'=> $this->string()->comment('КПП'),
            'official_address'=> $this->string()->comment('Юридический адрес'),
            'address_equals'=> $this->boolean()->defaultValue(false)->comment('Фактический адрес совпадает с юридическим'),
            'director'=> $this->string()->comment('Генеральный директор'),
            'email' => $this->string()->comment('Email'),
            'phone' => $this->string()->comment('Телефон'),
            'site'=> $this->string()->comment('Сайт'),
            'bank_bik' => $this->string()->comment('БИК/SWIFT'),
            'bank_name' => $this->string()->comment('Наименование банка'),
            'bank_address' => $this->string()->comment('Адрес банка'),
            'bank_correspondent_account' => $this->string()->comment('Корреспондентский счёт'),
            'bank_register_number' => $this->string()->comment('Регистрационный номер'),
            'bank_registration_date' => $this->date()->comment('Дата регистрации'),
            'bank_payment_account' => $this->string()->comment('Расчетный счет'),

            'rating'=> $this->integer()->comment('Рейтинг'),
            'api_key' => $this->string()->comment('АПИ ключ'),
        ]);
        $this->insert('company', [
            'name' => 'Супер компания',
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('company');
    }
}
