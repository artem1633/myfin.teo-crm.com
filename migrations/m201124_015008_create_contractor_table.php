<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contractor`.
 */
class m201124_015008_create_contractor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contractor', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'type' => $this->integer()->comment('Тип компании'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-contractor-company_id',
            'contractor',
            'company_id'
        );

        $this->addForeignKey(
            'fk-contractor-company_id',
            'contractor',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-contractor-company_id',
            'contractor'
        );

        $this->dropIndex(
            'idx-contractor-company_id',
            'contractor'
        );
        $this->dropTable('contractor');
    }
}
