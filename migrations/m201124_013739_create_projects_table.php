<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 */
class m201124_013739_create_projects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'comment' => $this->text()->comment('Примечания'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-projects-company_id',
            'projects',
            'company_id'
        );

        $this->addForeignKey(
            'fk-projects-company_id',
            'projects',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-projects-company_id',
            'projects'
        );

        $this->dropIndex(
            'idx-projects-company_id',
            'projects'
        );
        $this->dropTable('projects');
    }
}
