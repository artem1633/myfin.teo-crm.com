<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ticket_message`.
 */
class m200602_235523_create_ticket_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ticket_message', [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer()->comment('Тикет'),
            'text' => $this->text()->comment('Текст'),
            'from' => $this->integer()->comment('Кто отправил'),
            'created_at' => $this->dateTime()->comment('Дата и время')
        ]);

        $this->createIndex(
            'idx-ticket_message-ticket_id',
            'ticket_message',
            'ticket_id'
        );

        $this->addForeignKey(
            'fk-ticket_message-ticket_id',
            'ticket_message',
            'ticket_id',
            'ticket',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-ticket_message-ticket_id',
            'ticket_message'
        );

        $this->dropIndex(
            'idx-ticket_message-ticket_id',
            'ticket_message'
        );

        $this->dropTable('ticket_message');
    }
}
