<?php

use yii\db\Migration;

/**
 * Handles the creation of table `invoice`.
 */
class m201124_020212_create_invoice_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('invoice', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'comment' => $this->text()->comment('Примечания'),
            'company_id' => $this->integer(),
            'customer_id' => $this->integer()->comment('Юридическое лицо')
        ]);
        $this->createIndex(
            'idx-invoice-company_id',
            'invoice',
            'company_id'
        );

        $this->addForeignKey(
            'fk-invoice-company_id',
            'invoice',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-invoice-customer_id',
            'invoice',
            'customer_id'
        );

        $this->addForeignKey(
            'fk-invoice-customer_id',
            'invoice',
            'customer_id',
            'customers',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-invoice-company_id',
            'invoice'
        );

        $this->dropIndex(
            'idx-invoice-company_id',
            'invoice'
        );
        $this->dropForeignKey(
            'fk-invoice-customer_id',
            'invoice'
        );

        $this->dropIndex(
            'idx-invoice-customer_id',
            'invoice'
        );
        $this->dropTable('invoice');
    }
}
