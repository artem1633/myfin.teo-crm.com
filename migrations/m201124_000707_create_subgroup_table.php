<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subgruop`.
 */
class m201124_000707_create_subgroup_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subgroup', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'group_id' => $this->integer()->comment('Группа'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-subgroup-group_id',
            'subgroup',
            'group_id'
        );

        $this->addForeignKey(
            'fk-subgroup-group_id',
            'subgroup',
            'group_id',
            'group',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-subgroup-company_id',
            'subgroup',
            'company_id'
        );

        $this->addForeignKey(
            'fk-subgroup-company_id',
            'subgroup',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-subgroup-company_id',
            'subgroup'
        );

        $this->dropIndex(
            'idx-subgroup-company_id',
            'subgroup'
        );
        $this->dropForeignKey(
            'fk-subgroup-group_id',
            'subgroup'
        );

        $this->dropIndex(
            'idx-subgroup-group_id',
            'subgroup'
        );
        $this->dropTable('subgroup');
    }
}
