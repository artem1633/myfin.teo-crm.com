<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crm`.
 */
class m201124_022452_create_crm_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('crm', [
            'id' => $this->primaryKey(),
            'contractor_id' => $this->integer()->comment('Клиент'),
            'company_id' => $this->integer(),
            'services_id' => $this->integer()->comment('Сервис'),
            'project_desc' => $this->text()->comment('Суть проетка'),
            'pay_all' => $this->float()->comment('Оплата всего'),
            'payed' => $this->float()->comment('Оплата оплачено'),
            'pay_remainder' => $this->float()->comment('Оплата остаток'),
            'employee_pay_all' => $this->float()->comment('Сотрудник расчет всего'),
            'employee_payed' => $this->float()->comment('Сотрудник уже оплачено'),
            'employee_remainder' => $this->float()->comment('Сотрудник Долг'),
            'work_start' => $this->date()->comment('Дата начала работ'),
            'date_plan' => $this->date()->comment('Срок сдачи план'),
            'date_fact' => $this->date()->comment('Срок сдачи факт'),
            'status' => $this->string()->comment('Статус'),
            'status_date' => $this->date()->comment('Датастатуса'),
            'comment' => $this->text()->comment('Примечания'),
            'user_id' => $this->integer()->comment('Сотрудник/исполнитель')
        ]);
        $this->createIndex(
            'idx-crm-company_id',
            'crm',
            'company_id'
        );

        $this->addForeignKey(
            'fk-crm-company_id',
            'crm',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-crm-contractor_id',
            'crm',
            'contractor_id'
        );

        $this->addForeignKey(
            'fk-crm-contractor_id',
            'crm',
            'contractor_id',
            'contractor',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-crm-services_id',
            'crm',
            'services_id'
        );

        $this->addForeignKey(
            'fk-crm-services_id',
            'crm',
            'services_id',
            'services',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-crm-user_id',
            'crm',
            'user_id'
        );

        $this->addForeignKey(
            'fk-crm-user_id',
            'crm',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-crm-company_id',
            'crm'
        );

        $this->dropIndex(
            'idx-crm-company_id',
            'crm'
        );
        $this->dropForeignKey(
            'fk-crm-contractor_id',
            'crm'
        );

        $this->dropIndex(
            'idx-crm-contractor_id',
            'crm'
        );
        $this->dropForeignKey(
            'fk-crm-services_id',
            'crm'
        );

        $this->dropIndex(
            'idx-crm-services_id',
            'crm'
        );
        $this->dropForeignKey(
            'fk-crm-user_id',
            'crm'
        );

        $this->dropIndex(
            'idx-crm-user_id',
            'crm'
        );
        $this->dropTable('crm');
    }
}
