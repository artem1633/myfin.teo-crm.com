<?php

use yii\db\Migration;

/**
 * Handles the creation of table `position`.
 */
class m200602_234031_create_position_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('position', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
            'company_id' => $this->integer()->comment('Компания'),
        ]);

        $this->createIndex(
            'idx-position-company_id',
            'position',
            'company_id'
        );

        $this->addForeignKey(
            'fk-position-company_id',
            'position',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-position-company_id',
            'position'
        );

        $this->dropIndex(
            'idx-position-company_id',
            'position'
        );

        $this->dropTable('position');
    }
}
