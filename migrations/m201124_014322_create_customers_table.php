<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customers`.
 */
class m201124_014322_create_customers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customers', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'comment' => $this->text()->comment('Примечания'),
            'company_id' => $this->integer()
        ]);
        $this->createIndex(
            'idx-customers-company_id',
            'customers',
            'company_id'
        );

        $this->addForeignKey(
            'fk-customers-company_id',
            'customers',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-customers-company_id',
            'customers'
        );

        $this->dropIndex(
            'idx-customers-company_id',
            'customers'
        );
        $this->dropTable('customers');
    }
}
